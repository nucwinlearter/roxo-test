---
category:
- Advertisement
date: 2001-01-25T21:00:00.000+00:00
image: "/uploads/rent.jpg"
project_images:
- "/uploads/rent.jpg"
title: Umbrella For Rent
type: ''

---
#### Client's Problem:

**Needed a Rent sign** _for a red umbrella._

#### Project Description:

Client wanted it to be _bold, clean, crisp and edgy_. Client requested we follow the **elements and principles of design**. Notes about the project:

* It's a good sign for selling an umbrella
* It comes in red.
* It's great for putting in your lawn.

#### Result:

"Red Umbrella for rent sign works. Red umbrella got rented out." - Client

#### Reaction & Final thoughts:

"**For Rent Umbrella** _was fun to make._" - Artists