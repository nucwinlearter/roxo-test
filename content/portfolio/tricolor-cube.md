---
category:
- Design
date: 2021-01-23T08:00:00.000+00:00
image: "/uploads/med.jpg"
project_images:
- "/uploads/med.jpg"
title: Tricolor Cube

---
#### Client's Problem: 

Needed an image which could both look good and test the placement of images on the page.

#### Project description: 

This is the first image created for this site. Top of the cube is blue. Front of the cube is red. Right side of the cube is green.

#### Result: 

Successfully provided a solution to meet client's design aesthetic and function requirements.

#### Reaction and final thoughts: 

It looks nice & artist had fun.