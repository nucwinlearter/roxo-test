---
category:
- Advertisement
color: ''
date: 2021-01-23T08:00:00.000+00:00
image: "/uploads/sale.jpg"
project_images:
- "/uploads/sale.jpg"
title: Twigs For Sale Sign
type: ''

---
#### Client's Problem:

**Needed a Sale sign** _for twigs_. 

#### Project Description:

Client didn't give instructions about the written content on this portion of the page. Client only wanted to see how the sign would look on the site.

* Twigs for sale is intended as a post.
* The sign is made of paper.
* Twigs for sale will sell many twigs.

#### Result:

"Twigs for sale sign worked. It sold a lot of twigs. It just works." - Client

#### Reaction & Final thoughts:

"It's good." - Artists