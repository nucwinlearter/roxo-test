---
category:
- Design
color: ''
date: 2021-01-25T21:00:00.000+00:00
image: "/uploads/cube.jpg"
project_images:
- "/uploads/cube.jpg"
title: Bread Cube
type: ''

---
#### Client's Problem:

Needed cube to look like bread.

#### Project Description:

Client wanted to see how the cube would look on the site. Here are some of the cube's qualities:

* The cube is golden brown.
* The cube smells good.
* The cube is small, compact and fluffy.

#### Result:

"This cube **looks like bread**. _Smells good,_ if it existed." - Client

#### Reaction & Final thoughts:

"The cube could have been better, if it had a bite." - Artists