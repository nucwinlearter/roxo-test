---
title: About Us
date: 2018-07-12T12:19:33.000+00:00
heading: WE ARE ERRLAKE. A CREATIVE STUDIO IN CALIFORNIA.
description: We specialize in developing brand identities, marketing strategy, advertisement
  campaigns, website graphics & illustrations for businesses. Our clients take part
  in each phase of the design process with us.
expertise_title: Expertise
expertise_sectors:
- Customer Experience Design
- Digital Products
- Development
- Campaign & Content
- Employer Branding
- Animation & Motion Graphics
- Packaging & Product Design
- Retail & Spacial
- Print & Editorial Design
- Concept/Text
- Information Design

---
